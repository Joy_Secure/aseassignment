﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Data;

namespace ASEAssignement
{
    class Parser
    {

        Canvas myCanvas;
        Variable myVariable = new Variable();
        Method myMethod = new Method();
        int lineReturn = 0;
        int lineNum = 0;
        int loopStart;
        int numOfLoops = 0;
        bool checking = false;
        bool executeCode = true;        
        string expression = "";

        public Parser(Graphics g)
        {
            myCanvas = new Canvas(g);
        }

        /// <summary>
        /// Takes in a string of and breaks it down into an array of commands
        /// it then places the first array index into a string called command
        /// and then compares this string to commands to see if it is valid
        /// , if the string is not a command then it appends to the error text
        /// box notifying of an error. if the command requires parameters then the 
        /// seccond index of the array is split and converted into an integer 
        /// array for use in draw commands. 
        /// 
        /// After this it refreshes the canvas to make the drawring appear.
        /// </summary>
        public void Parse(string parse)
        {
            myCanvas.Clear();
            
            String[] script = parse.Split('\n');

            for (this.lineNum = 0; this.lineNum < script.Length; this.lineNum++)
            {
                Console.WriteLine("we are at line: " + lineNum);

                string line = script[lineNum].ToLower().Trim();                                        

                string[] sLine = line.Split(' ');

                string command = sLine[0];

                switch (command)
                {
                    case "endif":
                        executeCode = true;
                        break;
                    case "endwhile":
                        executeCode = true;
                        break;
                    case "endmeth":
                        executeCode = true;
                        break;
                }

                if(executeCode == false)
                {
                    continue;
                }

                Console.WriteLine("Is this a method: " + myMethod.IsMethod(command) + " ");

                if (myMethod.IsMethod(command) == true)
                {
                    lineReturn = lineNum + 1;
                    lineNum = myMethod.GetMethod(command);
                    Console.WriteLine("the line number is:" + lineNum + " and the line return is: " + lineReturn + " ");
                    continue;
                }

                //Console.WriteLine("Mid command");

                switch (command)
                {
                    case "colour":
                        string colour = sLine[1];
                        myCanvas.Colour(colour);
                        break;
                    case "fill":
                        myCanvas.Fill(sLine[1]);
                        break;
                    case "clear":
                        myCanvas.Clear();
                        break;
                    case "reset":
                        myCanvas.Reset();
                        break;
                    case "var":
                        try
                        {
                            if (sLine.Length == 4)
                            {
                                myVariable.SetVariable(sLine[1], int.Parse(sLine[3]));
                            }
                            else if (sLine.Length > 4)
                            {
                                string[] parVar = line.Split('=');
                                myVariable.SetVariable(sLine[1], myVariable.ParseVariable(parVar[1]));
                            }
                        }
                        catch (Exception exc)
                        {
                            System.Windows.Forms.MessageBox.Show("Invalid command at line: " + lineNum);
                            if (exc is IndexOutOfRangeException)
                            {
                                System.Windows.Forms.MessageBox.Show("Invalid declaration of variable at line:: " + lineNum);
                            }
                            if (exc is FormatException)
                            {
                                System.Windows.Forms.MessageBox.Show("Supplied Parameter is not a number at line: " + lineNum);
                            }
                            if(exc is InvalidOperator)
                            {
                                System.Windows.Forms.MessageBox.Show("Invalid operator at line: " + lineNum);
                            }
                        }
                        break;
                    case "meth":
                        try
                        {
                            int methodStart = lineNum;
                            myMethod.AddMethod(sLine[1], methodStart);
                            executeCode = false;
                            Console.WriteLine("Declaring method: " + myMethod.GetMethod(sLine[1]));
                        }
                        catch (Exception exc)
                        {
                            System.Windows.Forms.MessageBox.Show("Invalid command at line: " + lineNum);
                            if (exc is IndexOutOfRangeException)
                            {
                                System.Windows.Forms.MessageBox.Show("Method does not have a valid name at line: " + lineNum);
                            }
                        }
                        break;
                    case "while":
                        try
                        {
                            for (int i = 1; i < sLine.Length; i++)
                            {
                                this.expression = this.expression + " " + sLine[i];
                            }

                            if (myVariable.CheckExpression(expression) == true)
                            {
                                loopStart = lineNum;
                            }
                            else
                            {
                                this.expression = "";
                                executeCode = false;
                            }
                        }
                        catch (Exception exc)
                        {
                            System.Windows.Forms.MessageBox.Show("Invalid command at line: " + lineNum);
                            if (exc is IndexOutOfRangeException)
                            {
                                System.Windows.Forms.MessageBox.Show("Invalid expression for while loop at line: " + lineNum);
                            }
                            if (exc is FormatException)
                            {
                                System.Windows.Forms.MessageBox.Show("Invalid parameter for expression of while loop at line: : " + lineNum);
                            }
                            if (exc is InvalidOperator)
                            {
                                System.Windows.Forms.MessageBox.Show("Invalid Comparitor for while loop at line: " + lineNum);
                            }
                        }
                        break;
                    case "if":
                        try
                        {
                            for (int i = 1; i < sLine.Length; i++)
                            {
                                this.expression = this.expression + " " + sLine[i];
                            }
                            Console.WriteLine(expression);

                            if (myVariable.CheckExpression(expression) == true)
                            {
                                this.expression = "";
                                continue;
                            }
                            else
                            {
                                this.expression = "";
                                System.Windows.Forms.MessageBox.Show("Invalid if statement");
                                executeCode = false;
                            }
                            
                        }
                        catch (Exception exc)
                        {
                            System.Windows.Forms.MessageBox.Show("Invalid command at line: " + lineNum);
                            if (exc is IndexOutOfRangeException)
                            {
                                System.Windows.Forms.MessageBox.Show("Invalid expression for while loop at line: " + lineNum);
                            }
                            if (exc is FormatException)
                            {
                                System.Windows.Forms.MessageBox.Show("Invalid parameter for expression of while loop at line: : " + lineNum);
                            }
                            if (exc is InvalidOperator)
                            {
                                System.Windows.Forms.MessageBox.Show("Invalid Comparitor for while loop at line: " + lineNum);
                            }
                        }
                        break;
                    case "endmeth":
                        if (lineReturn != 0)
                        {
                            lineNum = lineReturn;
                        }
                        break;
                    case "endwhile":
                        if (executeCode == true)
                        {
                            if(myVariable.CheckExpression(this.expression) == true)
                            {
                                lineNum = loopStart;
                            }
                            else
                            {
                                this.expression = "";
                                continue;
                            }
                        }
                        this.executeCode = true;
                        break;
                    case "endif":
                        this.expression = "";
                        this.executeCode = true;
                        break;
                    case "end":
                        if (numOfLoops != 0)
                        {
                            lineNum = this.loopStart;
                            numOfLoops--;
                        }
                        break;
                    default:
                        try
                        {
                            //taking in the second array index and attempting to place it in an integer array for use in draw commands

                            string[] param = sLine[1].Split(',');

                            int[] intParam = new int[param.Length];

                            for (int i = 0; i < param.Length; i++)
                            {
                                param[i] = param[i].Trim();
                                if (myVariable.IsVariable(param[i]) == true)
                                { 
                                    intParam[i] = myVariable.GetVariable(param[i]);
                                }
                                else
                                {
                                    intParam[i] = int.Parse(param[i]);
                                }
                            }
                            
                            switch (command)
                            {
                                case "drawto":
                                    int x = intParam[0];
                                    int y = intParam[1];
                                    myCanvas.DrawLine(x, y);
                                    break;
                                case "moveto":
                                    int x2 = intParam[0];
                                    int y2 = intParam[1];
                                    myCanvas.MovePen(x2, y2);
                                    break;
                                case "triangle":
                                    int width = intParam[0];
                                    myCanvas.Triangle(width);
                                    break;
                                case "circle":
                                    int radius = intParam[0];
                                    myCanvas.Circle(radius);
                                    Console.WriteLine("DRAWING CIRCLE");
                                    break;
                                case "rect":
                                    int width2 = intParam[0];
                                    int height = intParam[1];
                                    myCanvas.Rectangle(width2, height);
                                    break;
                                case "loop":
                                    this.loopStart = lineNum;
                                    numOfLoops = intParam[0];
                                    break;
                                default:
                                    System.Windows.Forms.MessageBox.Show("Invalid command at line: " + lineNum);
                                    break;
                            }
                        }
                        catch (Exception exc)
                        {
                            System.Windows.Forms.MessageBox.Show("Invalid command at line: " + lineNum);

                            if (exc is IndexOutOfRangeException)
                            {
                                System.Windows.Forms.MessageBox.Show("Invalid number of parameters at line: " + lineNum);
                            }

                            if (exc is FormatException)
                            {
                                System.Windows.Forms.MessageBox.Show("Supplied Parameter is not a number at line: " + lineNum);
                            }

                        }
                        break;

                }
                //Console.WriteLine("End command");
                if (checking == true)
                {
                    myCanvas.Clear();
                    myCanvas.MovePen(0, 0);
                }


            }

            myVariable.Clear();
            myMethod.Clear();
        }

        public void SetCheckingFalse()
        {
            checking = false;
        }

        public void SetCheckingTrue()
        {
            checking = true;
        }
    }
}
