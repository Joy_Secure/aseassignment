﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAssignement
{
    class Canvas
    {

        Graphics g;
        Pen pen;
        private SolidBrush brush = new SolidBrush(Color.Black);
        int xStart, yStart;
        bool fill = false;


        /// <summary> 
        /// Constructer for the class
        /// This takes in a drawring surface g for use in methods
        /// and creates the pen and brush objects that are used for drawing.
        /// </summary>
        public Canvas(Graphics g)
        {

            this.g = g;
            xStart = yStart = 0;
            pen = new Pen(Color.Black, 1);
            SolidBrush brush = new SolidBrush(Color.Black);
        }

        /// <summary>
        /// This simply clears the drawring area g
        /// and erases any previous drawrings
        /// </summary>
        public void Clear()
        {
            g.Clear(Color.Transparent);

        }

        /// <summary>
        /// Draw line takes in two integers and uses them
        /// to draw a line between the currently stored x and y 
        /// coordinates and the two integers passed to it
        /// </summary>
        public void DrawLine(int xEnd, int yEnd)
        {

            g.DrawLine(pen, xStart, yStart, xEnd, yEnd);
            xStart = xEnd;
            yStart = yEnd;

        }

        /// <summary>
        /// This up-dates the coordinates stored in the canvas class
        /// with two integers passed when the method is called
        /// </summary>
        public void MovePen (int xEnd, int yEnd)
        {

            xStart = xEnd;
            yStart = yEnd;

        }

        /// <summary>
        /// This resets the stored x and y coordinates to there
        /// starting value of 0.
        /// </summary>
        public void Reset()
        {

            xStart = 0;
            yStart = 0;

        }

        /// <summary>
        /// This paints a rectangle to the drawring surface of a size equal to the 
        /// given width and height. 
        /// 
        /// The output of this method changes depending on whether or not the fill 
        /// boolean is true or false.
        /// </summary>
        public void Rectangle(int width, int height)
        {
            if (this.fill == true)
            {
                g.FillRectangle(brush, xStart, yStart, width, height);
            }
            else
            {
                g.DrawRectangle(pen, xStart, yStart, width, height);
            }
        }

        /// <summary>
        /// This paints a triangle to the drawring surface using a given width and 
        /// an array of points based on the width and currtent x and y coordinates.
        /// 
        /// The output of this method changes depending on whether or not the fill 
        /// boolean is true or false.
        /// </summary>
        public void Triangle(int width)
        {
            Point[] triangle = new Point[] { new Point(xStart, yStart - (width / 2)), new Point(xStart + (width / 2), yStart + (width / 2)), new Point(xStart - (width / 2), yStart + (width / 2)) };

            if (this.fill == true)
            {
                g.FillPolygon(brush, triangle);
            }
            else
            {
                g.DrawPolygon(pen, triangle);
            }
        }

        /// <summary>
        /// This paints a circle to the drawring surface using a given radius.
        /// 
        /// The output of this method changes depending on whether or not the fill 
        /// boolean is true or false.
        /// </summary>
        public void Circle(int radius)
        {
            if (this.fill == true)
            {
                g.FillEllipse(brush, xStart, yStart, xStart + (radius * 2), yStart + (radius * 2));
            }else
            {
                g.DrawEllipse(pen, xStart, yStart, xStart + (radius * 2), yStart + (radius * 2));
            }
        }

        /// <summary>
        /// Changes the colour of both the ben and the brush objects based
        /// on a given string "colour"
        /// </summary>
        public void Colour(string colour)
        {
            switch (colour)
            {
                case "black":
                    pen.Color = Color.Black;
                    brush.Color = Color.Black;
                    break;
                case "green":
                    pen.Color = Color.Green;
                    brush.Color = Color.Green;
                    break;
                case "blue":
                    pen.Color = Color.Blue;
                    brush.Color = Color.Blue;
                    break;
                case "red":
                    pen.Color = Color.Red;
                    brush.Color = Color.Red;
                    break;
                case "purple":
                    pen.Color = Color.Purple;
                    brush.Color = Color.Purple;
                    break;
                case "indigo":
                    pen.Color = Color.Indigo;
                    brush.Color = Color.Indigo;
                    break;
            }
        }

        /// <summary>
        /// Sets the fill boolean to true or false depending on the option supplied
        /// </summary>
        public void Fill(string option)
        {
            switch (option)
            {
                case "on":
                    this.fill = true;
                    break;
                case "off":
                    this.fill = false;
                    break;
                
            }
        }
    }
}
