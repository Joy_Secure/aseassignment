﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ASEAssignement

{
    public partial class Form1 : Form
    {

        Bitmap outputBitmap = new Bitmap(477, 500);
        SaveFileDialog saveDialog = new SaveFileDialog();
        Parser P;

        public Form1()
        {
            InitializeComponent();
            P = new Parser(Graphics.FromImage(outputBitmap));
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// when enter is pressed in the text box this passes the contents
        /// as a string to the parser method and then clears the text box.
        /// </summary>
        private void ComandLine_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode == Keys.Enter)
            {
                string phrase = ComandLine.Text;
                P.Parse(phrase);
                ComandLine.Text = "";
                Refresh();
            }
        }


        private void Output_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImageUnscaled(outputBitmap, 0, 0);
        }
        
        /// <summary>
        /// This splits all the data in the rich text box into an array and
        /// interates through each line passing them to the parser method.
        /// </summary>
        private void Run_button_MouseClick(object sender, MouseEventArgs e)
        {

            string script = ScriptEditor.Text;
            P.SetCheckingFalse();
            P.Parse(script);

            Refresh();

        }

        /// <summary?
        /// Opens a file chooser dialog and then writes the rich tex box contents
        /// to the selected file.
        /// </summary>
        private void Save_Buttpn_MouseClick(object sender, MouseEventArgs e)
        {

            SaveFileDialog save = new SaveFileDialog();
            save.Title = "Save your Program";
            save.Filter = "Text Files (*.txt)|*.txt";


            if (save.ShowDialog() == DialogResult.OK)
            {

                StreamWriter write = new StreamWriter(File.Create(save.FileName));
                write.Write(ScriptEditor.Text);
                write.Dispose();

            }
            
        }

        /// <summary>
        /// Opens a file chooser dialog and reads from the chosen file to the
        /// rich text box.
        /// </summary>
        private void LoadButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Title = "Load a file";
            open.Filter = "Text Files (*.txt)|*.txt";

            if (open.ShowDialog() == DialogResult.OK)
            {

                StreamReader read = new StreamReader(File.OpenRead(open.FileName));

                ScriptEditor.Text = read.ReadToEnd();

                read.Dispose();

            }
        }

        private void Syntax_Click(object sender, EventArgs e)
        {
            string script = ScriptEditor.Text;
            P.SetCheckingTrue();
            P.Parse(script);

            Refresh();
        }
    }
}
