﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAssignement
{
    class Method
    {
        Dictionary<string, int> meth = new Dictionary<string, int>();

        public void Clear()
        {
            meth.Clear();
        }

        public void AddMethod(string methName, int methLineNum)
        {
            if(this.IsMethod(methName) == true)
            {
                meth.Remove(methName);
                meth.Add(methName, methLineNum);
            }
            else
            {
                meth.Add(methName, methLineNum);
            }
        }

        public int GetMethod(string methName)
        {
            return meth[methName];
        }

        public bool IsMethod(string potMeth)
        {
            foreach (var item in meth.Keys)
            {
                if (item.Equals(potMeth) == true)
                {
                    return true;
                }
            }
            return false;
        }


    }
}
