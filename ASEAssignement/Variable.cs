﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASEAssignement
{

    class Variable 
    {

        Dictionary<string, int> VAR = new Dictionary<string, int>();

        public void Clear()
        {
            VAR.Clear();
        }

        public void SetVariable(string varName, int varValue)
        {
            if (this.IsVariable(varName) == true)
            {
                VAR.Remove(varName);
                VAR.Add(varName, varValue);
            }
            else
            {
                VAR.Add(varName, varValue);
            }
            
        }

        public int GetVariable(string varName)
        {

            return VAR[varName];

        }

        public bool IsVariable(string potVar)
        {

            potVar = potVar.Trim();
            
            foreach (var item in VAR.Keys)
            {
                if (item.Equals(potVar) == true)
                {
                    return true;
                }


            }
            return false;

        }

        public int ParseVariable(string parVar)
        {
            int var1;
            int var2;
            int result;
            parVar = parVar.Trim();
            string[] toParse = parVar.Split(' ');
            char op = char.Parse(toParse[1]);

            if(this.IsVariable(toParse[0]) == true)
            {
                var1 = this.GetVariable(toParse[0]);
            }
            else
            {
                var1 = int.Parse(toParse[0]);
            }

            if(this.IsVariable(toParse[2]) == true)
            {
                var2 = this.GetVariable(toParse[2]);
            }
            else
            {
                var2 = int.Parse(toParse[2]);
            }

            switch (op)
            {
                case '+':
                    result = var1 + var2;
                    return result;
                case '-':
                    result = var1 - var2;
                    return result;
                case '*':
                    result = var1 * var2;
                    return result;
                case '/':
                    result = var1 / var2;
                    return result;
                default:
                    throw new InvalidOperator();
            }







        }

        public bool CheckExpression(string expression)
        {
            expression = expression.Trim();
            string[] toParse = expression.Split(' ');
            char op = char.Parse(toParse[1]);
            int var1;
            int var2;

            if (this.IsVariable(toParse[0]) == true)
            {
                var1 = this.GetVariable(toParse[0]);
            }
            else
            {
                var1 = int.Parse(toParse[0]);
            }

            if (this.IsVariable(toParse[2]) == true)
            {
                var2 = this.GetVariable(toParse[2]);
            }
            else
            {
                var2 = int.Parse(toParse[2]);
            }

            switch (op)
            {
                case '<':
                    if(var1 < var2)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }                   
                case '>':
                    if (var1 > var2)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                default:
                    throw new InvalidOperator();
            }
        }
    }
}
