﻿namespace ASEAssignement
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComandLine = new System.Windows.Forms.TextBox();
            this.ScriptEditor = new System.Windows.Forms.RichTextBox();
            this.Output = new System.Windows.Forms.PictureBox();
            this.Run_button = new System.Windows.Forms.Button();
            this.Save_Buttpn = new System.Windows.Forms.Button();
            this.LoadButton = new System.Windows.Forms.Button();
            this.CommandLineLabel = new System.Windows.Forms.Label();
            this.ScriptEditorLabel = new System.Windows.Forms.Label();
            this.OutputLabel = new System.Windows.Forms.Label();
            this.Syntax = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.Output)).BeginInit();
            this.SuspendLayout();
            // 
            // ComandLine
            // 
            this.ComandLine.Location = new System.Drawing.Point(250, 550);
            this.ComandLine.Name = "ComandLine";
            this.ComandLine.Size = new System.Drawing.Size(500, 20);
            this.ComandLine.TabIndex = 0;
            this.ComandLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComandLine_KeyDown);
            // 
            // ScriptEditor
            // 
            this.ScriptEditor.Location = new System.Drawing.Point(12, 28);
            this.ScriptEditor.Name = "ScriptEditor";
            this.ScriptEditor.Size = new System.Drawing.Size(480, 500);
            this.ScriptEditor.TabIndex = 1;
            this.ScriptEditor.Text = "";
            // 
            // Output
            // 
            this.Output.BackColor = System.Drawing.Color.Gray;
            this.Output.Location = new System.Drawing.Point(511, 28);
            this.Output.Name = "Output";
            this.Output.Size = new System.Drawing.Size(477, 500);
            this.Output.TabIndex = 2;
            this.Output.TabStop = false;
            this.Output.Paint += new System.Windows.Forms.PaintEventHandler(this.Output_Paint);
            // 
            // Run_button
            // 
            this.Run_button.Location = new System.Drawing.Point(459, 588);
            this.Run_button.Name = "Run_button";
            this.Run_button.Size = new System.Drawing.Size(75, 23);
            this.Run_button.TabIndex = 4;
            this.Run_button.Text = "Run";
            this.Run_button.UseVisualStyleBackColor = true;
            this.Run_button.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Run_button_MouseClick);
            // 
            // Save_Buttpn
            // 
            this.Save_Buttpn.Location = new System.Drawing.Point(768, 548);
            this.Save_Buttpn.Name = "Save_Buttpn";
            this.Save_Buttpn.Size = new System.Drawing.Size(75, 23);
            this.Save_Buttpn.TabIndex = 5;
            this.Save_Buttpn.Text = "Save";
            this.Save_Buttpn.UseVisualStyleBackColor = true;
            this.Save_Buttpn.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Save_Buttpn_MouseClick);
            // 
            // LoadButton
            // 
            this.LoadButton.Location = new System.Drawing.Point(154, 548);
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(75, 23);
            this.LoadButton.TabIndex = 6;
            this.LoadButton.Text = "Load";
            this.LoadButton.UseVisualStyleBackColor = true;
            this.LoadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // CommandLineLabel
            // 
            this.CommandLineLabel.AutoSize = true;
            this.CommandLineLabel.Location = new System.Drawing.Point(250, 531);
            this.CommandLineLabel.Name = "CommandLineLabel";
            this.CommandLineLabel.Size = new System.Drawing.Size(77, 13);
            this.CommandLineLabel.TabIndex = 8;
            this.CommandLineLabel.Text = "Command Line";
            // 
            // ScriptEditorLabel
            // 
            this.ScriptEditorLabel.AutoSize = true;
            this.ScriptEditorLabel.Location = new System.Drawing.Point(12, 9);
            this.ScriptEditorLabel.Name = "ScriptEditorLabel";
            this.ScriptEditorLabel.Size = new System.Drawing.Size(88, 13);
            this.ScriptEditorLabel.TabIndex = 9;
            this.ScriptEditorLabel.Text = "Program Window";
            // 
            // OutputLabel
            // 
            this.OutputLabel.AutoSize = true;
            this.OutputLabel.Location = new System.Drawing.Point(511, 8);
            this.OutputLabel.Name = "OutputLabel";
            this.OutputLabel.Size = new System.Drawing.Size(89, 13);
            this.OutputLabel.TabIndex = 10;
            this.OutputLabel.Text = "Command Output";
            // 
            // Syntax
            // 
            this.Syntax.Location = new System.Drawing.Point(459, 617);
            this.Syntax.Name = "Syntax";
            this.Syntax.Size = new System.Drawing.Size(75, 23);
            this.Syntax.TabIndex = 11;
            this.Syntax.Text = "Syntax";
            this.Syntax.UseVisualStyleBackColor = true;
            this.Syntax.Click += new System.EventHandler(this.Syntax_Click);
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(1000, 750);
            this.Controls.Add(this.Syntax);
            this.Controls.Add(this.OutputLabel);
            this.Controls.Add(this.ScriptEditorLabel);
            this.Controls.Add(this.CommandLineLabel);
            this.Controls.Add(this.LoadButton);
            this.Controls.Add(this.Save_Buttpn);
            this.Controls.Add(this.Run_button);
            this.Controls.Add(this.Output);
            this.Controls.Add(this.ScriptEditor);
            this.Controls.Add(this.ComandLine);
            this.Name = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.Output)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ComandLine;
        private System.Windows.Forms.RichTextBox ScriptEditor;
        private System.Windows.Forms.PictureBox Output;
        private System.Windows.Forms.Button Run_button;
        private System.Windows.Forms.Button Save_Buttpn;
        private System.Windows.Forms.Button LoadButton;
        private System.Windows.Forms.Label CommandLineLabel;
        private System.Windows.Forms.Label ScriptEditorLabel;
        private System.Windows.Forms.Label OutputLabel;
        private System.Windows.Forms.Button Syntax;
    }
}

